#pragma once

#include <stdbool.h>
#include <stdint.h>

/* Forward declaration. */
typedef struct z80c_mmu z80c_mmu_t;

/* Some GameBoy's CPU registers can be accessed as 16-bits or two 8-bits
 * registers. The following type exposes such registers by abstracting
 * the endianness of the current platform.
 */
typedef union {
	/* Access as 2x8-bits registers. For instance for the BC register, 'l'
	 * refers to B and 'h' refers to C.
	 */
	struct {
/* TODO(sgosselin):
 * Add support for different endianness. For bring-up, assume the host is a
 * big-endian machine.
 */
#if 0
		uint8_t l, h;
#else
		uint8_t h, l;
#endif
	} u8;
	/* Access as 16-bits register. */
	uint16_t u16;
} z80c_cpu_reg16_t;

/** GameBoy's CPU flags. */
typedef enum {
	/* Set when the result of a math operation is zero or two values match
	 * when using the CP instruction.
	 */
	Z80C_CPU_FLAG_Z = (1 << 7),
	/* Set if a substraction was performed in the last math instruction. */
	Z80C_CPU_FLAG_N = (1 << 6),
	/* Set if a carry occured from the lower nibble in the last math
	 * operation.
	 */
	Z80C_CPU_FLAG_H = (1 << 5),
	/* Set if a carry occured from the last math operation or if register A
	 * is the smaller value when executing the CP instruction.
	 */
	Z80C_CPU_FLAG_C = (1 << 4),
} z80c_cpu_flag_t;

/** GameBoy's CPU registers. */
typedef struct {
	z80c_cpu_reg16_t af;
	z80c_cpu_reg16_t bc;
	z80c_cpu_reg16_t de;
	z80c_cpu_reg16_t hl;
	uint16_t sp;
	uint16_t pc;
	uint8_t  flags;
} z80c_cpu_regs_t;

/** Opaque object that represents the GameBoy's CPU. */
typedef struct z80c_cpu z80c_cpu_t;

/**
 * Create a z80c_cpu_t object.
 *
 * The caller owns the object and is responsible for calling z80c_cpu_destroy().
 */
z80c_cpu_t *z80c_cpu_create(void);

/**
 * Destroy a z80c_cpu_t object.
 *
 * Upon return, the caller must ensure the object is not used anymore.
 */
void z80c_cpu_destroy(z80c_cpu_t *cpu);

/**
 * Reset a z80c_cpu_t object to its initial state.
 */
void z80c_cpu_reset(z80c_cpu_t *cpu);

/**
 * Step the CPU by doing a fetch/decode/execute cycle.
 *
 * @return
 * Number of clock cycles spent to fetch/decode/execute the next instruction.
 */
unsigned int z80c_cpu_step(z80c_cpu_t *cpu, z80c_mmu_t *mmu);

