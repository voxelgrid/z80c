#include "bios.h"
#include "log.h"
#include "mmu.h"
#include "ppu.h"

#include <stdlib.h>

/* Address to unmap the BIOS from memory. */
#define ADDR_UNMAP_BIOS (0xFF50)

/* Memory region of the BIOS. */
#define ADDR_BEG_BIOS (0x0000)
#define ADDR_END_BIOS (0x00FF)

/* Memory region of the video RAM. */
#define ADDR_BEG_VRAM (0x8000)
#define ADDR_END_VRAM (0x9FFF)

/* Memory region of the working RAM. */
#define ADDR_BEG_WRAM (0xC000)
#define ADDR_END_WRAM (0xDFFF)

/* Memory region of the shadow copy of the working RAM. */
#define ADDR_BEG_SHADOW_WRAM (0xE000)
#define ADDR_END_SHADOW_WRAM (0xFDFF)

/* Memory region of the Zero-RAM. */
#define ADDR_BEG_ZRAM (0xFF80)
#define ADDR_END_ZRAM (0xFFFF)

typedef struct z80c_mmu {
	uint8_t const *bios;
	uint8_t        vram[ADDR_END_VRAM - ADDR_BEG_VRAM + 1];
	uint8_t        wram[ADDR_END_WRAM - ADDR_BEG_WRAM + 1];
	uint8_t        zram[ADDR_END_ZRAM - ADDR_BEG_ZRAM + 1];
} z80c_mmu_t;

z80c_mmu_t *
z80c_mmu_create(void)
{
	z80c_mmu_t *mmu;

	if ((mmu = calloc(1, sizeof(*mmu))) == NULL) {
		Z80C_LOGE("calloc() failed, reason: %s\n", strerror(errno));
		return NULL;
	}

	z80c_mmu_reset(mmu);

	return mmu;
}

void
z80c_mmu_destroy(z80c_mmu_t *mmu)
{
	if (mmu) {
		free(mmu);
	}
}

void
z80c_mmu_reset(z80c_mmu_t *mmu)
{
	Z80C_ASSERT(mmu);

	/* BIOS is mapped. */
	mmu->bios = z80c_bios_get(NULL);

	/* Clear WRAM and ZRAM. */
	(void)memset(mmu->wram, 0, sizeof(mmu->wram));
	(void)memset(mmu->zram, 0, sizeof(mmu->wram));
}

bool
z80c_mmu_is_bios_mapped(z80c_mmu_t *mmu)
{
	Z80C_ASSERT(mmu);

	return (mmu->bios != NULL);
}

void
z80c_mmu_write_u8(z80c_mmu_t *mmu, uint16_t addr, uint8_t u8)
{
	Z80C_ASSERT(mmu);

	if ((addr == ADDR_UNMAP_BIOS) && (u8 != 0) && mmu->bios) {
		mmu->bios = NULL;
		Z80C_LOGD("BIOS unmapped\n");
	} else if ((addr >= ADDR_BEG_VRAM) && (addr <= ADDR_END_VRAM)) {
		mmu->vram[addr - ADDR_BEG_VRAM] = u8;
	} else if ((addr >= ADDR_BEG_WRAM) && (addr <= ADDR_END_WRAM)) {
		mmu->wram[addr - ADDR_BEG_WRAM] = u8;
	} else if ((addr >= ADDR_BEG_SHADOW_WRAM) && (addr <= ADDR_END_SHADOW_WRAM)) {
		mmu->wram[addr - ADDR_BEG_SHADOW_WRAM] = u8;
	} else if ((addr >= ADDR_BEG_ZRAM) && (addr <= ADDR_END_ZRAM)) {
		mmu->zram[addr - ADDR_BEG_ZRAM] = u8;
	}

	Z80C_LOGD("mmu-write u8=%02X addr=%04X\n", u8, addr);
}

void
z80c_mmu_write_u16(z80c_mmu_t *mmu, uint16_t addr, uint16_t u16)
{
	Z80C_ASSERT(mmu);

	z80c_mmu_write_u8(mmu, addr, u16 & 0xFF);
	z80c_mmu_write_u8(mmu, addr + 1, (u16 >> 8) & 0xFF);
}

uint8_t
z80c_mmu_read_u8(z80c_mmu_t *mmu, uint16_t addr)
{
	Z80C_ASSERT(mmu);

	 if (addr == ADDR_UNMAP_BIOS) {
		return mmu->bios ? 0x00 : 0x01;
	} else if ((addr >= ADDR_BEG_BIOS) && (addr <= ADDR_END_BIOS) && mmu->bios) {
		return mmu->bios[addr - ADDR_BEG_BIOS];
	} else if ((addr >= ADDR_BEG_VRAM) && (addr <= ADDR_END_VRAM)) {
		return mmu->vram[addr - ADDR_BEG_VRAM];
	} else if ((addr >= ADDR_BEG_WRAM) && (addr <= ADDR_END_WRAM)) {
		return mmu->wram[addr - ADDR_BEG_WRAM];
	} else if ((addr >= ADDR_BEG_SHADOW_WRAM) && (addr <= ADDR_END_SHADOW_WRAM)) {
		return mmu->wram[addr - ADDR_BEG_SHADOW_WRAM];
	} else if ((addr >= ADDR_BEG_ZRAM) && (addr <= ADDR_END_ZRAM)) {
		return mmu->zram[addr - ADDR_BEG_ZRAM];
	}

	return 0x00;
}

uint16_t
z80c_mmu_read_u16(z80c_mmu_t *mmu, uint16_t addr)
{
	uint16_t res = 0;

	Z80C_ASSERT(mmu);

	res |= z80c_mmu_read_u8(mmu, addr + 1);
	res <<= 8;
	res |= z80c_mmu_read_u8(mmu, addr);

	return res;
}

