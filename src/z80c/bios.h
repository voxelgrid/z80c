#pragma once

#include <stdint.h>
#include <stdlib.h>

/**
 * Get a pointer on the BIOS.
 *
 * @param[out] bios_nbytes This will be populated with the number of bytes
 *                         consumed by the BIOS.
 * @return Pointer on the region of memory containing the BIOS.
 */
uint8_t const *z80c_bios_get(size_t *bios_nbytes);

