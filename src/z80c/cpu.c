#include "cpu.h"
#include "mmu.h"
#include "log.h"

#include <assert.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>

/* GameBoy's opcodes are encoded on 8-bits. There are 256 opcodes per
 * instruction set.
 */
#define NB_OPCODE_PER_TABLE (256)

/* Macro that start the definition of an opcode. */
#define OPCODE_IMPL(name) \
	static unsigned int _opcode_##name(z80c_cpu_t *cpu, z80c_mmu_t *mmu)

/* Macro that returns the address of an opcode implementation. */
#define OPCODE_ADDR(name) \
	&_opcode_##name

typedef struct z80c_cpu {
	z80c_cpu_regs_t regs;
} z80c_cpu_t;

typedef struct {
	char const *desc;
	unsigned int (*func)(z80c_cpu_t *cpu, z80c_mmu_t *mmu);
} z80c_opcode_t;

/* GameBoy's CPU has two instruction sets: classic and extended, also called the
 * 0xCB instruction set. Two opcode tables are available and will be used during
 * the opcode decoding phase.
 *
 * References:
 * http://www.chrisantonellis.com/files/gameboy/gb-instructions.txt
 * http://www.devrs.com/gb/files/opcodes.html
 * http://www.pastraiser.com/cpu/gameboy/gameboy_opcodes.html
 */
static const z80c_opcode_t OPCODE_TABLE[NB_OPCODE_PER_TABLE];
static const z80c_opcode_t OPCODE_TABLE_CB[NB_OPCODE_PER_TABLE];

static _Noreturn void
_z80c_cpu_fault(z80c_cpu_t *cpu, z80c_mmu_t *mmu, uint8_t op,
	z80c_opcode_t const *opcode_table)
{
	Z80C_ASSERT(cpu);
	Z80C_ASSERT(mmu);
	Z80C_ASSERT(opcode_table);

	Z80C_LOGE("\n");
	Z80C_LOGE("=========================================\n");
	Z80C_LOGE("CPU FAULT!\n");
	Z80C_LOGE("\tA=0x%02X F=0x%02X\n", cpu->regs.af.u8.l, cpu->regs.af.u8.h);
	Z80C_LOGE("\tB=0x%02X C=0x%02X\n", cpu->regs.bc.u8.l, cpu->regs.bc.u8.h);
	Z80C_LOGE("\tD=0x%02X E=0x%02X\n", cpu->regs.de.u8.l, cpu->regs.de.u8.h);
	Z80C_LOGE("\tH=0x%02X L=0x%02X\n", cpu->regs.hl.u8.l, cpu->regs.hl.u8.h);
	Z80C_LOGE("\tSP=0x%04X\n", cpu->regs.sp);
	Z80C_LOGE("\tPC=0x%04X\n", cpu->regs.pc);
	Z80C_LOGE("\tFL=0x%02X\n", cpu->regs.flags);
	Z80C_LOGE("\tOP=0x%02X (CB: %1d)\n", op, (opcode_table == OPCODE_TABLE_CB));
	Z80C_LOGE("=========================================\n");

	/* Trap to debugger if possible. */
	abort();
}

static inline bool
_z80c_cpu_flag_is_set(z80c_cpu_t *cpu, z80c_cpu_flag_t flag)
{
	return (cpu->regs.flags & flag) != 0;
}

static inline void
_z80c_cpu_flag_set(z80c_cpu_t *cpu, z80c_cpu_flag_t flag, bool is_set)
{
    if (is_set)
        cpu->regs.flags |= flag;
    else
        cpu->regs.flags &= ~flag;
}

z80c_cpu_t *
z80c_cpu_create(void)
{
	z80c_cpu_t *cpu = NULL;

	if ((cpu = calloc(1, sizeof(*cpu))) == NULL) {
		Z80C_LOGE("calloc() failed, reason: %s\n", strerror(errno));
		return NULL;
	}

	z80c_cpu_reset(cpu);

	return cpu;
}

void
z80c_cpu_destroy(z80c_cpu_t *cpu)
{
	if (cpu) {
		free(cpu);
	}
}

void
z80c_cpu_reset(z80c_cpu_t *cpu)
{
	Z80C_ASSERT(cpu);

	/* Initialize CPU registers to their default value. */
	cpu->regs.pc = 0x0000;
	cpu->regs.sp = 0x0000;
}

unsigned int
z80c_cpu_step(z80c_cpu_t *cpu, z80c_mmu_t *mmu)
{
	z80c_opcode_t const *opcode_table = NULL;
	uint16_t origin_pc = 0;
	uint8_t op = 0;

	Z80C_ASSERT(cpu);
	Z80C_ASSERT(mmu);

	/* Keep track of the current program counter as decoding/executing
	 * the opcode will modify it.
	 */
	origin_pc = cpu->regs.pc;

	/* Fetch next opcode. */
	op = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;

	/* If the opcode is part of the extended instruction set, fetch the
	 * next opcode. In any cases, determine what instruction set table
	 * must be used to decode the opcode.
	 */
	if (op == 0xCB) {
		/* Fetch next opcode. */
		op = z80c_mmu_read_u8(mmu, cpu->regs.pc);
		cpu->regs.pc++;
		/* Use the extended instruction set. */
		opcode_table = OPCODE_TABLE_CB;
	} else {
		/* Use the non-extended instruction set. */
		opcode_table = OPCODE_TABLE;
	}

	/* Fault if the opcode has not been implemented yet. The function is not
	 * expected to return.
	 */
	if (!opcode_table[op].func) {
		_z80c_cpu_fault(cpu, mmu, op, opcode_table);
	}

	/* Execute the opcode/instruction. */
	Z80C_LOGD("pc:0x%04X inst: %s\n", origin_pc, opcode_table[op].desc);
	return opcode_table[op].func(cpu, mmu);
}


static void
_opcode_BIT_b_d8(z80c_cpu_t *cpu, uint8_t d8, uint8_t b)
{
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_Z, (d8 & (1 << b)) == 0);
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_N, false);
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_H, true);
}

static void
_opcode_INC_r8(z80c_cpu_t *cpu, uint8_t *r8)
{
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_H, (*r8 & 0x0f) == 0x0f);
	*r8 += 1;
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_Z, (*r8 == 0));
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_N, false);
}

static void
_opcode_RL_r8(z80c_cpu_t *cpu, uint8_t *r8)
{
	uint8_t oldcarry = _z80c_cpu_flag_is_set(cpu, Z80C_CPU_FLAG_C) ? 1 : 0;

	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_C, (*r8 >> 7) != 0);
	*r8 = (*r8 << 1) | oldcarry;
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_N, false);
	_z80c_cpu_flag_set(cpu, Z80C_CPU_FLAG_Z, false);
}

static void
_opcode_SET_b_r8(z80c_cpu_t *cpu, uint8_t *r8, uint8_t b)
{
    *r8 |= (1 << b);
}

static void
_opcode_XOR_d8(z80c_cpu_t *cpu, uint8_t d8)
{
	cpu->regs.af.u8.l ^= d8;
	cpu->regs.flags = cpu->regs.af.u8.l ? 0x00: Z80C_CPU_FLAG_Z;
}

OPCODE_IMPL(CALL_a16) {
	z80c_mmu_write_u16(mmu, cpu->regs.sp - 2, cpu->regs.pc + 2);
	cpu->regs.sp = cpu->regs.sp - 2;
	cpu->regs.pc = z80c_mmu_read_u16(mmu, cpu->regs.pc);
	return 24;
}

OPCODE_IMPL(INC_B) { _opcode_INC_r8(cpu, &cpu->regs.bc.u8.l); return 4; }
OPCODE_IMPL(INC_C) { _opcode_INC_r8(cpu, &cpu->regs.bc.u8.h); return 4; }
OPCODE_IMPL(INC_D) { _opcode_INC_r8(cpu, &cpu->regs.de.u8.l); return 4; }
OPCODE_IMPL(INC_E) { _opcode_INC_r8(cpu, &cpu->regs.de.u8.h); return 4; }
OPCODE_IMPL(INC_H) { _opcode_INC_r8(cpu, &cpu->regs.hl.u8.l); return 4; }
OPCODE_IMPL(INC_L) { _opcode_INC_r8(cpu, &cpu->regs.hl.u8.h); return 4; }
OPCODE_IMPL(INC_A) { _opcode_INC_r8(cpu, &cpu->regs.af.u8.l); return 4; }

OPCODE_IMPL(LD_B_B) { cpu->regs.bc.u8.l = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_B_C) { cpu->regs.bc.u8.l = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_B_D) { cpu->regs.bc.u8.l = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_B_E) { cpu->regs.bc.u8.l = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_B_H) { cpu->regs.bc.u8.l = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_B_L) { cpu->regs.bc.u8.l = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_B_A) { cpu->regs.bc.u8.l = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_C_B) { cpu->regs.bc.u8.h = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_C_C) { cpu->regs.bc.u8.h = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_C_D) { cpu->regs.bc.u8.h = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_C_E) { cpu->regs.bc.u8.h = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_C_H) { cpu->regs.bc.u8.h = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_C_L) { cpu->regs.bc.u8.h = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_C_A) { cpu->regs.bc.u8.h = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_D_B) { cpu->regs.de.u8.l = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_D_C) { cpu->regs.de.u8.l = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_D_D) { cpu->regs.de.u8.l = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_D_E) { cpu->regs.de.u8.l = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_D_H) { cpu->regs.de.u8.l = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_D_L) { cpu->regs.de.u8.l = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_D_A) { cpu->regs.de.u8.l = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_E_B) { cpu->regs.de.u8.h = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_E_C) { cpu->regs.de.u8.h = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_E_D) { cpu->regs.de.u8.h = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_E_E) { cpu->regs.de.u8.h = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_E_H) { cpu->regs.de.u8.h = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_E_L) { cpu->regs.de.u8.h = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_E_A) { cpu->regs.de.u8.h = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_H_B) { cpu->regs.hl.u8.l = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_H_C) { cpu->regs.hl.u8.l = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_H_D) { cpu->regs.hl.u8.l = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_H_E) { cpu->regs.hl.u8.l = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_H_H) { cpu->regs.hl.u8.l = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_H_L) { cpu->regs.hl.u8.l = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_H_A) { cpu->regs.hl.u8.l = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_L_B) { cpu->regs.hl.u8.h = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_L_C) { cpu->regs.hl.u8.h = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_L_D) { cpu->regs.hl.u8.h = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_L_E) { cpu->regs.hl.u8.h = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_L_H) { cpu->regs.hl.u8.h = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_L_L) { cpu->regs.hl.u8.h = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_L_A) { cpu->regs.hl.u8.h = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_A_B) { cpu->regs.af.u8.l = cpu->regs.bc.u8.l; return 4; }
OPCODE_IMPL(LD_A_C) { cpu->regs.af.u8.l = cpu->regs.bc.u8.h; return 4; }
OPCODE_IMPL(LD_A_D) { cpu->regs.af.u8.l = cpu->regs.de.u8.l; return 4; }
OPCODE_IMPL(LD_A_E) { cpu->regs.af.u8.l = cpu->regs.de.u8.h; return 4; }
OPCODE_IMPL(LD_A_H) { cpu->regs.af.u8.l = cpu->regs.hl.u8.l; return 4; }
OPCODE_IMPL(LD_A_L) { cpu->regs.af.u8.l = cpu->regs.hl.u8.h; return 4; }
OPCODE_IMPL(LD_A_A) { cpu->regs.af.u8.l = cpu->regs.af.u8.l; return 4; }

OPCODE_IMPL(LD_A_d8) {
	cpu->regs.af.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;
	return 8;
}

OPCODE_IMPL(LD_B_d8) {
	cpu->regs.bc.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;
	return 8;
}

OPCODE_IMPL(LD_C_d8) {
	cpu->regs.bc.u8.h = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;
	return 8;
}


OPCODE_IMPL(LD_A_aBC) {
	cpu->regs.af.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.bc.u16);
	return 8;
}

OPCODE_IMPL(LD_A_aDE) {
	cpu->regs.af.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.de.u16);
	return 8;
}

OPCODE_IMPL(LD_A_aHLI) {
	cpu->regs.af.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.hl.u16);
	cpu->regs.hl.u16++;
	return 8;
}

OPCODE_IMPL(LD_A_aHLD) {
	cpu->regs.af.u8.l = z80c_mmu_read_u8(mmu, cpu->regs.hl.u16);
	cpu->regs.hl.u16--;
	return 8;
}

OPCODE_IMPL(LD_IOd8_A) {
	uint8_t d8 = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;
	z80c_mmu_write_u8(mmu, 0xFF00 + d8, cpu->regs.af.u8.l);
	return 12;
}

OPCODE_IMPL(LD_IOC_A) {
	z80c_mmu_write_u8(mmu, 0xFF00 + cpu->regs.bc.u8.h, cpu->regs.af.u8.l);
	return 8;
}

OPCODE_IMPL(LD_BC_d16) {
	cpu->regs.bc.u16 = z80c_mmu_read_u16(mmu, cpu->regs.pc);
	cpu->regs.pc += 2;
	return 12;
}

OPCODE_IMPL(LD_DE_d16) {
	cpu->regs.de.u16 = z80c_mmu_read_u16(mmu, cpu->regs.pc);
	cpu->regs.pc += 2;
	return 12;
}

OPCODE_IMPL(LD_HL_d16) {
	cpu->regs.hl.u16 = z80c_mmu_read_u16(mmu, cpu->regs.pc);
	cpu->regs.pc += 2;
	return 12;
}

OPCODE_IMPL(LD_aHL_A) {
	z80c_mmu_write_u8(mmu, cpu->regs.hl.u16, cpu->regs.af.u8.l);
	return 8;
}

OPCODE_IMPL(LD_HLI_A) {
	z80c_mmu_write_u8(mmu, cpu->regs.hl.u16, cpu->regs.af.u8.l);
	cpu->regs.hl.u16++;
	return 8;
}

OPCODE_IMPL(LD_HLD_A) {
	z80c_mmu_write_u8(mmu, cpu->regs.hl.u16, cpu->regs.af.u8.l);
	cpu->regs.hl.u16--;
	return 8;
}

OPCODE_IMPL(LD_SP_d16) {
	cpu->regs.sp = z80c_mmu_read_u16(mmu, cpu->regs.pc);
	cpu->regs.pc += 2;
	return 12;
}

OPCODE_IMPL(PUSH_BC) {
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 1, cpu->regs.bc.u8.l);
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 2, cpu->regs.bc.u8.h);
	cpu->regs.sp -= 2;
	return 16;
}

OPCODE_IMPL(PUSH_DE) {
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 1, cpu->regs.de.u8.l);
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 2, cpu->regs.de.u8.h);
	cpu->regs.sp -= 2;
	return 16;
}

OPCODE_IMPL(PUSH_HL) {
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 1, cpu->regs.hl.u8.l);
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 2, cpu->regs.hl.u8.h);
	cpu->regs.sp -= 2;
	return 16;
}

OPCODE_IMPL(PUSH_AF) {
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 1, cpu->regs.af.u8.l);
	z80c_mmu_write_u8(mmu, cpu->regs.sp - 2, cpu->regs.af.u8.h);
	cpu->regs.sp -= 2;
	return 16;
}

OPCODE_IMPL(XOR_B)  { _opcode_XOR_d8(cpu, cpu->regs.bc.u8.l); return 4; }
OPCODE_IMPL(XOR_C)  { _opcode_XOR_d8(cpu, cpu->regs.bc.u8.h); return 4; }
OPCODE_IMPL(XOR_D)  { _opcode_XOR_d8(cpu, cpu->regs.de.u8.l); return 4; }
OPCODE_IMPL(XOR_E)  { _opcode_XOR_d8(cpu, cpu->regs.de.u8.h); return 4; }
OPCODE_IMPL(XOR_H)  { _opcode_XOR_d8(cpu, cpu->regs.hl.u8.l); return 4; }
OPCODE_IMPL(XOR_L)  { _opcode_XOR_d8(cpu, cpu->regs.hl.u8.h); return 4; }
OPCODE_IMPL(XOR_A)  { _opcode_XOR_d8(cpu, cpu->regs.af.u8.l); return 4; }

OPCODE_IMPL(BIT_0_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 0); return 8; }
OPCODE_IMPL(BIT_1_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 1); return 8; }
OPCODE_IMPL(BIT_2_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 2); return 8; }
OPCODE_IMPL(BIT_3_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 3); return 8; }
OPCODE_IMPL(BIT_4_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 4); return 8; }
OPCODE_IMPL(BIT_5_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 5); return 8; }
OPCODE_IMPL(BIT_6_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 6); return 8; }
OPCODE_IMPL(BIT_7_A) { _opcode_BIT_b_d8(cpu, cpu->regs.af.u8.l, 7); return 8; }

OPCODE_IMPL(BIT_0_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 0); return 8; }
OPCODE_IMPL(BIT_1_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 1); return 8; }
OPCODE_IMPL(BIT_2_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 2); return 8; }
OPCODE_IMPL(BIT_3_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 3); return 8; }
OPCODE_IMPL(BIT_4_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 4); return 8; }
OPCODE_IMPL(BIT_5_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 5); return 8; }
OPCODE_IMPL(BIT_6_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 6); return 8; }
OPCODE_IMPL(BIT_7_B) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.l, 7); return 8; }

OPCODE_IMPL(BIT_0_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 0); return 8; }
OPCODE_IMPL(BIT_1_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 1); return 8; }
OPCODE_IMPL(BIT_2_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 2); return 8; }
OPCODE_IMPL(BIT_3_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 3); return 8; }
OPCODE_IMPL(BIT_4_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 4); return 8; }
OPCODE_IMPL(BIT_5_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 5); return 8; }
OPCODE_IMPL(BIT_6_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 6); return 8; }
OPCODE_IMPL(BIT_7_C) { _opcode_BIT_b_d8(cpu, cpu->regs.bc.u8.h, 7); return 8; }

OPCODE_IMPL(BIT_0_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 0); return 8; }
OPCODE_IMPL(BIT_1_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 1); return 8; }
OPCODE_IMPL(BIT_2_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 2); return 8; }
OPCODE_IMPL(BIT_3_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 3); return 8; }
OPCODE_IMPL(BIT_4_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 4); return 8; }
OPCODE_IMPL(BIT_5_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 5); return 8; }
OPCODE_IMPL(BIT_6_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 6); return 8; }
OPCODE_IMPL(BIT_7_D) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.l, 7); return 8; }

OPCODE_IMPL(BIT_0_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 0); return 8; }
OPCODE_IMPL(BIT_1_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 1); return 8; }
OPCODE_IMPL(BIT_2_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 2); return 8; }
OPCODE_IMPL(BIT_3_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 3); return 8; }
OPCODE_IMPL(BIT_4_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 4); return 8; }
OPCODE_IMPL(BIT_5_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 5); return 8; }
OPCODE_IMPL(BIT_6_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 6); return 8; }
OPCODE_IMPL(BIT_7_E) { _opcode_BIT_b_d8(cpu, cpu->regs.de.u8.h, 7); return 8; }

OPCODE_IMPL(BIT_0_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 0); return 8; }
OPCODE_IMPL(BIT_1_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 1); return 8; }
OPCODE_IMPL(BIT_2_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 2); return 8; }
OPCODE_IMPL(BIT_3_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 3); return 8; }
OPCODE_IMPL(BIT_4_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 4); return 8; }
OPCODE_IMPL(BIT_5_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 5); return 8; }
OPCODE_IMPL(BIT_6_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 6); return 8; }
OPCODE_IMPL(BIT_7_H) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.l, 7); return 8; }

OPCODE_IMPL(BIT_0_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 0); return 8; }
OPCODE_IMPL(BIT_1_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 1); return 8; }
OPCODE_IMPL(BIT_2_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 2); return 8; }
OPCODE_IMPL(BIT_3_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 3); return 8; }
OPCODE_IMPL(BIT_4_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 4); return 8; }
OPCODE_IMPL(BIT_5_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 5); return 8; }
OPCODE_IMPL(BIT_6_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 6); return 8; }
OPCODE_IMPL(BIT_7_L) { _opcode_BIT_b_d8(cpu, cpu->regs.hl.u8.h, 7); return 8; }

OPCODE_IMPL(BIT_0_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 0);
	return 16;
}

OPCODE_IMPL(BIT_1_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 1);
	return 16;
}

OPCODE_IMPL(BIT_2_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 2);
	return 16;
}

OPCODE_IMPL(BIT_3_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 3);
	return 16;
}

OPCODE_IMPL(BIT_4_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 4);
	return 16;
}

OPCODE_IMPL(BIT_5_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 5);
	return 16;
}

OPCODE_IMPL(BIT_6_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 6);
	return 16;
}

OPCODE_IMPL(BIT_7_HL) {
	_opcode_BIT_b_d8(cpu, z80c_mmu_read_u8(mmu, cpu->regs.hl.u16), 7);
	return 16;
}

OPCODE_IMPL(RL_A) { _opcode_RL_r8(cpu, &cpu->regs.af.u8.l); return 8; }
OPCODE_IMPL(RL_B) { _opcode_RL_r8(cpu, &cpu->regs.bc.u8.l); return 8; }
OPCODE_IMPL(RL_C) { _opcode_RL_r8(cpu, &cpu->regs.bc.u8.h); return 8; }
OPCODE_IMPL(RL_D) { _opcode_RL_r8(cpu, &cpu->regs.de.u8.l); return 8; }
OPCODE_IMPL(RL_E) { _opcode_RL_r8(cpu, &cpu->regs.de.u8.h); return 8; }
OPCODE_IMPL(RL_H) { _opcode_RL_r8(cpu, &cpu->regs.hl.u8.l); return 8; }
OPCODE_IMPL(RL_L) { _opcode_RL_r8(cpu, &cpu->regs.hl.u8.h); return 8; }

OPCODE_IMPL(SET_0_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 0); return 8; }
OPCODE_IMPL(SET_1_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 1); return 8; }
OPCODE_IMPL(SET_2_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 2); return 8; }
OPCODE_IMPL(SET_3_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 3); return 8; }
OPCODE_IMPL(SET_4_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 4); return 8; }
OPCODE_IMPL(SET_5_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 5); return 8; }
OPCODE_IMPL(SET_6_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 6); return 8; }
OPCODE_IMPL(SET_7_A) { _opcode_SET_b_r8(cpu, &cpu->regs.af.u8.l, 7); return 8; }

OPCODE_IMPL(SET_0_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 0); return 8; }
OPCODE_IMPL(SET_1_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 1); return 8; }
OPCODE_IMPL(SET_2_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 2); return 8; }
OPCODE_IMPL(SET_3_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 3); return 8; }
OPCODE_IMPL(SET_4_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 4); return 8; }
OPCODE_IMPL(SET_5_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 5); return 8; }
OPCODE_IMPL(SET_6_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 6); return 8; }
OPCODE_IMPL(SET_7_B) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.l, 7); return 8; }

OPCODE_IMPL(SET_0_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 0); return 8; }
OPCODE_IMPL(SET_1_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 1); return 8; }
OPCODE_IMPL(SET_2_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 2); return 8; }
OPCODE_IMPL(SET_3_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 3); return 8; }
OPCODE_IMPL(SET_4_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 4); return 8; }
OPCODE_IMPL(SET_5_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 5); return 8; }
OPCODE_IMPL(SET_6_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 6); return 8; }
OPCODE_IMPL(SET_7_C) { _opcode_SET_b_r8(cpu, &cpu->regs.bc.u8.h, 7); return 8; }

OPCODE_IMPL(SET_0_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 0); return 8; }
OPCODE_IMPL(SET_1_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 1); return 8; }
OPCODE_IMPL(SET_2_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 2); return 8; }
OPCODE_IMPL(SET_3_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 3); return 8; }
OPCODE_IMPL(SET_4_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 4); return 8; }
OPCODE_IMPL(SET_5_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 5); return 8; }
OPCODE_IMPL(SET_6_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 6); return 8; }
OPCODE_IMPL(SET_7_D) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.l, 7); return 8; }

OPCODE_IMPL(SET_0_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 0); return 8; }
OPCODE_IMPL(SET_1_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 1); return 8; }
OPCODE_IMPL(SET_2_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 2); return 8; }
OPCODE_IMPL(SET_3_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 3); return 8; }
OPCODE_IMPL(SET_4_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 4); return 8; }
OPCODE_IMPL(SET_5_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 5); return 8; }
OPCODE_IMPL(SET_6_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 6); return 8; }
OPCODE_IMPL(SET_7_E) { _opcode_SET_b_r8(cpu, &cpu->regs.de.u8.h, 7); return 8; }

OPCODE_IMPL(SET_0_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 0); return 8; }
OPCODE_IMPL(SET_1_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 1); return 8; }
OPCODE_IMPL(SET_2_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 2); return 8; }
OPCODE_IMPL(SET_3_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 3); return 8; }
OPCODE_IMPL(SET_4_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 4); return 8; }
OPCODE_IMPL(SET_5_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 5); return 8; }
OPCODE_IMPL(SET_6_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 6); return 8; }
OPCODE_IMPL(SET_7_H) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.l, 7); return 8; }

OPCODE_IMPL(SET_0_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 0); return 8; }
OPCODE_IMPL(SET_1_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 1); return 8; }
OPCODE_IMPL(SET_2_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 2); return 8; }
OPCODE_IMPL(SET_3_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 3); return 8; }
OPCODE_IMPL(SET_4_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 4); return 8; }
OPCODE_IMPL(SET_5_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 5); return 8; }
OPCODE_IMPL(SET_6_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 6); return 8; }
OPCODE_IMPL(SET_7_L) { _opcode_SET_b_r8(cpu, &cpu->regs.hl.u8.h, 7); return 8; }

OPCODE_IMPL(JR_NZ_d8) {
	int8_t sd8 = z80c_mmu_read_u8(mmu, cpu->regs.pc);
	cpu->regs.pc++;
	if ((cpu->regs.flags & Z80C_CPU_FLAG_Z) != 0)
		return 8;
	cpu->regs.pc += sd8;
	return 12;
}

/* Opcodes table for the classic instruction set. */
static const z80c_opcode_t OPCODE_TABLE[NB_OPCODE_PER_TABLE] = {
	[0x01] = { "LD BC, d16",  OPCODE_ADDR(LD_BC_d16) },
	[0x04] = { "INC B",       OPCODE_ADDR(INC_B)     },
	[0x06] = { "LD B, d8",    OPCODE_ADDR(LD_B_d8)   },
	[0x0C] = { "INC C",       OPCODE_ADDR(INC_C)     },
	[0x0E] = { "LD C, d8",    OPCODE_ADDR(LD_C_d8)   },
	[0x11] = { "LD DE, d16",  OPCODE_ADDR(LD_DE_d16) },
	[0x14] = { "INC D",       OPCODE_ADDR(INC_D)     },
	[0x1A] = { "LD A, (DE)",  OPCODE_ADDR(LD_A_aDE)  },
	[0x1C] = { "INC E",       OPCODE_ADDR(INC_E)     },
	[0x20] = { "JR NZ, d8",   OPCODE_ADDR(JR_NZ_d8)  },
	[0x21] = { "LD HL, d16",  OPCODE_ADDR(LD_HL_d16) },
	[0x24] = { "INC H",       OPCODE_ADDR(INC_H)     },
	[0x2C] = { "INC L",       OPCODE_ADDR(INC_L)     },
	[0x31] = { "LD SP, d16",  OPCODE_ADDR(LD_SP_d16) },
	[0x32] = { "LD (HL-), A", OPCODE_ADDR(LD_HLD_A)  },
	[0x3C] = { "INC A",       OPCODE_ADDR(INC_A)     },
	[0x3E] = { "LD A, d8",    OPCODE_ADDR(LD_A_d8)   },
	[0x4F] = { "LD C, A",     OPCODE_ADDR(LD_C_A)    },
	[0x77] = { "LD (HL), A",  OPCODE_ADDR(LD_aHL_A)  },
	[0xA8] = { "XOR B",       OPCODE_ADDR(XOR_B)     },
	[0xA9] = { "XOR C",       OPCODE_ADDR(XOR_C)     },
	[0xAA] = { "XOR D",       OPCODE_ADDR(XOR_D)     },
	[0xAB] = { "XOR E",       OPCODE_ADDR(XOR_E)     },
	[0xAC] = { "XOR H",       OPCODE_ADDR(XOR_H)     },
	[0xAD] = { "XOR L",       OPCODE_ADDR(XOR_L)     },
	[0xAF] = { "XOR A",       OPCODE_ADDR(XOR_A)     },
	[0xC5] = { "PUSH BC",     OPCODE_ADDR(PUSH_BC)   },
	[0xCD] = { "CALL a16",    OPCODE_ADDR(CALL_a16)  },
	[0xD5] = { "PUSH DE",     OPCODE_ADDR(PUSH_DE)   },
	[0xE0] = { "LD (d8), A",  OPCODE_ADDR(LD_IOd8_A) },
	[0xE2] = { "LD (C), A",   OPCODE_ADDR(LD_IOC_A)  },
	[0xE5] = { "PUSH HL",     OPCODE_ADDR(PUSH_HL)   },
	[0xF5] = { "PUSH AF",     OPCODE_ADDR(PUSH_AF)   },
	{0},
};

/* Opcodes table for the extended instruction set. */
static const z80c_opcode_t OPCODE_TABLE_CB[NB_OPCODE_PER_TABLE] = {
	[0x10] = {"RL B",         OPCODE_ADDR(RL_B)      },
	[0x11] = {"RL C",         OPCODE_ADDR(RL_C)      },
	[0x12] = {"RL D",         OPCODE_ADDR(RL_D)      },
	[0x13] = {"RL E",         OPCODE_ADDR(RL_E)      },
	[0x14] = {"RL H",         OPCODE_ADDR(RL_H)      },
	[0x15] = {"RL L",         OPCODE_ADDR(RL_L)      },
	[0x17] = {"RL A",         OPCODE_ADDR(RL_A)      },
	[0x7C] = {"BIT 7, H",     OPCODE_ADDR(BIT_7_H)   },
	[0xC0] = {"SET 0, B",     OPCODE_ADDR(SET_0_B)   },
	[0xC1] = {"SET 0, C",     OPCODE_ADDR(SET_0_C)   },
	[0xC2] = {"SET 0, D",     OPCODE_ADDR(SET_0_D)   },
	[0xC3] = {"SET 0, E",     OPCODE_ADDR(SET_0_E)   },
	[0xC4] = {"SET 0, H",     OPCODE_ADDR(SET_0_H)   },
	[0xC5] = {"SET 0, L",     OPCODE_ADDR(SET_0_L)   },
	[0xC7] = {"SET 0, A",     OPCODE_ADDR(SET_0_A)   },
	[0xC8] = {"SET 1, B",     OPCODE_ADDR(SET_1_B)   },
	[0xC9] = {"SET 1, C",     OPCODE_ADDR(SET_1_C)   },
	[0xCA] = {"SET 1, D",     OPCODE_ADDR(SET_1_D)   },
	[0xCB] = {"SET 1, E",     OPCODE_ADDR(SET_1_E)   },
	[0xCC] = {"SET 1, H",     OPCODE_ADDR(SET_1_H)   },
	[0xCD] = {"SET 1, L",     OPCODE_ADDR(SET_1_L)   },
	[0xCF] = {"SET 1, A",     OPCODE_ADDR(SET_1_A)   },
	[0xD0] = {"SET 2, B",     OPCODE_ADDR(SET_2_B)   },
	[0xD1] = {"SET 2, C",     OPCODE_ADDR(SET_2_C)   },
	[0xD2] = {"SET 2, D",     OPCODE_ADDR(SET_2_D)   },
	[0xD3] = {"SET 2, E",     OPCODE_ADDR(SET_2_E)   },
	[0xD4] = {"SET 2, H",     OPCODE_ADDR(SET_2_H)   },
	[0xD5] = {"SET 2, L",     OPCODE_ADDR(SET_2_L)   },
	[0xD7] = {"SET 2, A",     OPCODE_ADDR(SET_0_A)   },
	[0xD8] = {"SET 3, B",     OPCODE_ADDR(SET_3_B)   },
	[0xD9] = {"SET 3, C",     OPCODE_ADDR(SET_3_C)   },
	[0xDA] = {"SET 3, D",     OPCODE_ADDR(SET_3_D)   },
	[0xDB] = {"SET 3, E",     OPCODE_ADDR(SET_3_E)   },
	[0xDC] = {"SET 3, H",     OPCODE_ADDR(SET_3_H)   },
	[0xDD] = {"SET 3, L",     OPCODE_ADDR(SET_3_L)   },
	[0xDF] = {"SET 3, A",     OPCODE_ADDR(SET_3_A)   },
	[0xE0] = {"SET 4, B",     OPCODE_ADDR(SET_4_B)   },
	[0xE1] = {"SET 4, C",     OPCODE_ADDR(SET_4_C)   },
	[0xE2] = {"SET 4, D",     OPCODE_ADDR(SET_4_D)   },
	[0xE3] = {"SET 4, E",     OPCODE_ADDR(SET_4_E)   },
	[0xE4] = {"SET 4, H",     OPCODE_ADDR(SET_4_H)   },
	[0xE5] = {"SET 4, L",     OPCODE_ADDR(SET_4_L)   },
	[0xE7] = {"SET 4, A",     OPCODE_ADDR(SET_4_A)   },
	[0xE8] = {"SET 5, B",     OPCODE_ADDR(SET_5_B)   },
	[0xE9] = {"SET 5, C",     OPCODE_ADDR(SET_5_C)   },
	[0xEA] = {"SET 5, D",     OPCODE_ADDR(SET_5_D)   },
	[0xEB] = {"SET 5, E",     OPCODE_ADDR(SET_5_E)   },
	[0xEC] = {"SET 5, H",     OPCODE_ADDR(SET_5_H)   },
	[0xED] = {"SET 5, L",     OPCODE_ADDR(SET_5_L)   },
	[0xEF] = {"SET 5, A",     OPCODE_ADDR(SET_5_A)   },
	[0xF0] = {"SET 6, B",     OPCODE_ADDR(SET_6_B)   },
	[0xF1] = {"SET 6, C",     OPCODE_ADDR(SET_6_C)   },
	[0xF2] = {"SET 6, D",     OPCODE_ADDR(SET_6_D)   },
	[0xF3] = {"SET 6, E",     OPCODE_ADDR(SET_6_E)   },
	[0xF4] = {"SET 6, H",     OPCODE_ADDR(SET_6_H)   },
	[0xF5] = {"SET 6, L",     OPCODE_ADDR(SET_6_L)   },
	[0xF7] = {"SET 6, A",     OPCODE_ADDR(SET_6_A)   },
	[0xF8] = {"SET 7, B",     OPCODE_ADDR(SET_7_B)   },
	[0xF9] = {"SET 7, C",     OPCODE_ADDR(SET_7_C)   },
	[0xFA] = {"SET 7, D",     OPCODE_ADDR(SET_7_D)   },
	[0xFB] = {"SET 7, E",     OPCODE_ADDR(SET_7_E)   },
	[0xFC] = {"SET 7, H",     OPCODE_ADDR(SET_7_H)   },
	[0xFD] = {"SET 7, L",     OPCODE_ADDR(SET_7_L)   },
	[0xFF] = {"SET 7, A",     OPCODE_ADDR(SET_7_A)   },
};

