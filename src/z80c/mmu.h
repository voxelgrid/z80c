#pragma once

#include <stdbool.h>
#include <stdint.h>

/** Opaque object that represents the GameBoy's MMU. */
typedef struct z80c_mmu z80c_mmu_t;

/**
 * Create a z80c_mmu_t object.
 *
 * The caller owns the object and is responsible for calling z80c_mmu_destroy().
 */
z80c_mmu_t *z80c_mmu_create(void);

/**
 * Destroy a z80c_mmu_t object.
 *
 * Upon return, the caller must ensure the object is not used anymore.
 */
void z80c_mmu_destroy(z80c_mmu_t *mmu);

/**
 * Reset a z80c_mmu_t object to its initial state.
 */
void z80c_mmu_reset(z80c_mmu_t *mmu);

/**
 * Check if the BIOS/BootROM is still mapped.
 */
bool z80c_mmu_is_bios_mapped(z80c_mmu_t *mmu);

/**
 * Read a byte from memory.
 */
uint8_t z80c_mmu_read_u8(z80c_mmu_t *mmu, uint16_t addr);

/**
 * Read a word from memory.
 */
uint16_t z80c_mmu_read_u16(z80c_mmu_t *mmu, uint16_t addr);

/**
 * Write a byte in memory.
 */
void z80c_mmu_write_u8(z80c_mmu_t *mmu, uint16_t addr, uint8_t u8);

/**
 * Write a word in memory.
 */
void z80c_mmu_write_u16(z80c_mmu_t *mmu, uint16_t addr, uint16_t u16);

