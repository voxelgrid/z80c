#pragma once

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#ifdef Z80C_DEBUG
#define Z80C_LOGD(fmt, ...) \
	fprintf(stdout, fmt, ##__VA_ARGS__)
#else
#define Z80C_LOGD(fmt, ...)
#endif

#define Z80C_LOGE(fmt, ...) \
	fprintf(stderr, fmt, ##__VA_ARGS__)

#define Z80C_LOGI(fmt, ...) \
	fprintf(stdout, fmt, ##__VA_ARGS__)

#define Z80C_ASSERT(cond) \
	assert((cond))

