#include <z80c/bios.h>

#define TEST_NO_MAIN
#include <acutest.h>

void
test_z80c_bios(void)
{
	uint8_t const *bios;
	size_t bios_nbytes;

	bios = z80c_bios_get(NULL);
	TEST_CHECK(bios != NULL);

	(void)z80c_bios_get(&bios_nbytes);
	TEST_CHECK(bios_nbytes != 0);
}

