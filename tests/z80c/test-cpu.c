#include <z80c/bios.h>
#include <z80c/cpu.h>
#include <z80c/mmu.h>

#define TEST_NO_MAIN
#include <acutest.h>

void
test_z80c_cpu(void)
{
	z80c_cpu_t *cpu = NULL;
	z80c_mmu_t *mmu = NULL;
	size_t bios_nbytes = 0;

	cpu = z80c_cpu_create();
	TEST_CHECK(cpu != NULL);

	mmu = z80c_mmu_create();
	TEST_CHECK(mmu != NULL);

	/* Now make sure the boot sequence can be executed completely. For this,
	 * fetch the number of bytes contained in the BIOS.
	 */
	(void)z80c_bios_get(&bios_nbytes);

	while (z80c_mmu_is_bios_mapped(mmu)) {
		unsigned int ncycles = z80c_cpu_step(cpu, mmu);
		TEST_CHECK(ncycles > 0);
	}
}

