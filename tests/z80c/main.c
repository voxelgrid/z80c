#include <acutest.h>

extern void test_z80c_bios(void);
extern void test_z80c_cpu(void);
extern void test_z80c_mmu(void);

TEST_LIST = {
	{ "test_z80c_bios", test_z80c_bios },
	{ "test_z80c_cpu", test_z80c_cpu },
	{ "test_z80c_mmu", test_z80c_mmu },
	{ 0 }
};

